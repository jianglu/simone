#!/usr/bin/env bash

rm -rf build_dir
mkdir build_dir
cd build_dir

export STAGING_DIR=/home/jianglu/Developer/voicekit/OpenWrt-SDK-15.05.1-ramips-mt7688_gcc-4.8-linaro_uClibc-0.9.33.2.Linux-x86_64/staging_dir
cmake .. \
    -DCMAKE_TOOLCHAIN_FILE=../openwrt-15.05.cmake \
    -DOPENWRT_BUILD=1
make