set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 1)

set(STAGING_DIR $ENV{STAGING_DIR})
set(TOOLCHAIN_DIR ${STAGING_DIR}/toolchain-mipsel_24kec+dsp_gcc-4.8-linaro_uClibc-0.9.33.2)

# specify the cross compiler
set(CMAKE_C_COMPILER ${TOOLCHAIN_DIR}/bin/mipsel-openwrt-linux-gcc)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_DIR}/bin/mipsel-openwrt-linux-g++)

set(CMAKE_FIND_ROOT_PATH ${STAGING_DIR}/target-mipsel_24kec+dsp_uClibc-0.9.33.2)

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(BOOST_ROOT ${TOOLCHAIN_DIR})

#set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/STM32F303RETx_FLASH.ld)
#set(COMMON_FLAGS "-mcpu=cortex-m4 -mthumb -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16 -ffunction-sections -fdata-sections -g -fno-common -fmessage-length=0")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D__OPENWRT_WORKAROUND__=1")
#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D__OPENWRT_WORKAROUND__=1")
#set(CMAKE_EXE_LINKER_FLAGS "-Wl,-gc-sections -T ${LINKER_SCRIPT}")