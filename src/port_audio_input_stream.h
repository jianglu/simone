//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "RedundantCast"
#pragma ide diagnostic ignored "OCUnusedStructInspection"
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
#ifndef SIMONE_PORT_AUDIO_INPUT_STREAM_H
#define SIMONE_PORT_AUDIO_INPUT_STREAM_H

#include <boost/asio/basic_io_object.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio/buffer.hpp>

namespace portaudio {

/**
 * PortAudio 音频输入流
 * @tparam Service
 */
template<typename Service>
class BasicPortAudioInputStream
        : public boost::asio::basic_io_object<Service> {
public:
    explicit BasicPortAudioInputStream(boost::asio::io_service &io_service)
            : boost::asio::basic_io_object<Service>(io_service) {}

    /**
     * 同步读取
     */
    template<typename MutableBufferSequence>
    std::size_t read_some(const MutableBufferSequence &buffers,
                          boost::system::error_code &ec) {
        return this->get_service().read_some(this->get_implementation(), buffers, ec);
    }

    /**
     * 异步读取
     * @tparam MutableBufferSequence
     * @tparam ReadHandler
     * @param buffers
     * @param handler
     */
    template<typename MutableBufferSequence, typename ReadHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(ReadHandler,
                                  void (boost::system::error_code, std::size_t))
    async_read_some(const MutableBufferSequence &buffers,
                    BOOST_ASIO_MOVE_ARG(ReadHandler)handler) {
        // If you get an error on the following line it means that your handler does
        // not meet the documented type requirements for a ReadHandler.
        using boost::asio::handler_type;
        BOOST_ASIO_READ_HANDLER_CHECK(ReadHandler, handler) type_check;

        this->get_service().async_read_some(
                this->get_implementation(),
                buffers, BOOST_ASIO_MOVE_CAST(ReadHandler)(handler));
    }
};

/**
 * PortAudio 音频输入流实现
 * @tparam Service
 */
class PortAudioInputStreamImpl {
public:
    PortAudioInputStreamImpl();

    virtual ~PortAudioInputStreamImpl();

    virtual void destroy() {}

    template<typename MutableBufferSequence>
    std::size_t read_some(const MutableBufferSequence &buffers, boost::system::error_code &ec) {
        std::size_t read = 0;
        typename MutableBufferSequence::const_iterator it;
        for (it = buffers.begin(); it != buffers.end(); ++it) {
            std::size_t size = boost::asio::buffer_size(*it) / sizeof(int16_t);
            int16_t *buf_ptr = boost::asio::buffer_cast<int16_t *>(*it);
            read_frame(buf_ptr, size);
            read += size;
        }
        ec = boost::system::error_code();
        return read;
    }

    void read_frame(int16_t *buf, size_t frames);

private:
    void *stream_;
};

template<typename StreamImplementation = PortAudioInputStreamImpl>
class PortAudioInputStreamService
        : public boost::asio::io_service::service {
public:
    static boost::asio::io_service::id id;

    explicit PortAudioInputStreamService(boost::asio::io_service &owner)
            : boost::asio::io_service::service(owner),
              async_work_(new boost::asio::io_service::work(async_io_service_)),
              async_thread_(boost::bind(&boost::asio::io_service::run, &async_io_service_)) {}

    ~PortAudioInputStreamService() {
        async_work_.reset();
        async_io_service_.stop();
        async_thread_.join();
    }

    typedef boost::shared_ptr<StreamImplementation> implementation_type;

    void construct(implementation_type &impl) {
        impl.reset(new StreamImplementation);
    }

    void destroy(implementation_type &impl) {
        impl->destroy();
        impl.reset();
    }

    /**
     * 异步读取
     */
    template<typename ReadHandler, typename MutableBufferSequence>
    class ReadOperation {
    public:
        ReadOperation(implementation_type &impl,
                      boost::asio::io_service &io_service,
                      const MutableBufferSequence &buffers,
                      ReadHandler handler) :
                impl_(impl),
                io_service_(io_service),
                work_(io_service),
                buffers_(buffers),
                handler_(handler) {
        }

        void operator()() const {
            implementation_type impl = impl_.lock();
            if (impl) {
                boost::system::error_code ec;
                std::size_t transferred = impl->read_some(buffers_, ec);
                this->io_service_.post(boost::asio::detail::bind_handler(
                        handler_, ec, transferred));
            } else {
                this->io_service_.post(boost::asio::detail::bind_handler(
                        handler_, boost::asio::error::operation_aborted, 0));
            }
        }

    private:
        boost::weak_ptr<StreamImplementation> impl_;
        boost::asio::io_service &io_service_;
        boost::asio::io_service::work work_;
        const MutableBufferSequence buffers_;
        ReadHandler handler_;
    };

    template<typename MutableBufferSequence, typename ReadHandler>
    void async_read_some(implementation_type &impl,
                         const MutableBufferSequence &buffers,
                         BOOST_ASIO_MOVE_ARG(ReadHandler)handler) {
        this->async_io_service_.post(
                ReadOperation<ReadHandler, MutableBufferSequence>(
                        impl, this->get_io_service(), buffers, handler));
    }

private:
    void shutdown_service() override {
    }

    boost::asio::io_service async_io_service_;
    boost::scoped_ptr<boost::asio::io_service::work> async_work_;
    boost::thread async_thread_;
};

template<typename PortAudioInputStreamImpl>
boost::asio::io_service::id PortAudioInputStreamService<PortAudioInputStreamImpl>::id;

typedef BasicPortAudioInputStream<PortAudioInputStreamService<PortAudioInputStreamImpl>> PortAudioInputStream;

}


#endif //SIMONE_PORT_AUDIO_INPUT_STREAM_H

#pragma clang diagnostic pop