//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SIMONE_MAD_PLAY_H
#define SIMONE_MAD_PLAY_H

#include <memory>
#include <boost/process.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/process/pipe.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/asio.hpp>
#include <json.hpp>

class MadPlay : public std::enable_shared_from_this<MadPlay> {
public:
    typedef std::function<void()> WaitHandler;

    MadPlay(boost::asio::io_service &ios)
            : io_service_(ios),
              signal_set_(io_service_, SIGCHLD) {
    }

    void play(const std::string &audio_url, WaitHandler handler) {
        auto self(shared_from_this());

        handler_ = handler;
        std::vector<std::string> args;
        args.push_back("/usr/bin/net_play");
        args.push_back(audio_url);

        signal_set_.async_wait([self](const boost::system::error_code &, int) {
            ::wait(&self->status_);
            self->handler_();
        });

        boost::process::execute(
                boost::process::initializers::set_args(args),
                boost::process::initializers::notify_io_service(io_service_)
        );
    }

private:
    int status_;
    WaitHandler handler_;
    boost::asio::io_service &io_service_;
    boost::asio::signal_set signal_set_;
};

#endif //SIMONE_MAD_PLAY_H
