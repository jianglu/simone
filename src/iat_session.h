//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SIMONE_IAT_SESSION_H
#define SIMONE_IAT_SESSION_H

#include <cstdint>
#include <websocketpp/client.hpp>
#include <websocketpp/config/asio_client.hpp>
#include "speex_encoder.h"
#include "port_audio_input_stream.h"
#include "vad_detector.h"

typedef std::function<void(const std::string &)> get_result_handler;

class IATSession : public std::enable_shared_from_this<IATSession> {
public:
    IATSession(boost::asio::io_service &ios);

    void connect(get_result_handler f);

    void start(websocketpp::connection_hdl conn);

    void get_result(websocketpp::connection_hdl conn);

private:
    void async_read(websocketpp::connection_hdl conn);

    void on_audio_raw_frame(websocketpp::connection_hdl conn);

    void on_audio_encoded_frame(websocketpp::connection_hdl conn, size_t frame_size);

    void on_vad_detect_end(websocketpp::connection_hdl conn);

    void write_audio(websocketpp::connection_hdl conn, const uint8_t *data, size_t size, int state);

    void on_get_result(websocketpp::connection_hdl conn, const std::string &result);

private:
    boost::asio::io_service &io_service_;
    portaudio::PortAudioInputStream audio_stream_;
    speex::SpeexEncoder encoder_;
    vad::VadDetector vad_detector_;
    std::array<int16_t, 320> buf_;
    std::array<uint8_t, 640> encoded_buf_;

    websocketpp::client<websocketpp::config::asio_tls_client> client_;

    std::vector<uint8_t> send_buffer_;
    std::string state_;
    std::string session_id_;
    std::string result_;
    long synid_;
    bool recording_;
    get_result_handler get_result_handler_;
};

#endif //SIMONE_IAT_SESSION_H
