//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
#pragma ide diagnostic ignored "RedundantCast"
#pragma ide diagnostic ignored "OCUnusedStructInspection"
#ifndef SIMONE_SPEEX_ENCODER_H
#define SIMONE_SPEEX_ENCODER_H


#include <boost/asio/basic_io_object.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio/buffer.hpp>
#include <speex/speex.h>

namespace speex {

class SpeexEncoderImpl {
public:
    SpeexEncoderImpl();

    virtual ~SpeexEncoderImpl();

    virtual void destroy() {}

    template<typename ConstBufferSequence, typename MutableBufferSequence>
    std::size_t encode(const ConstBufferSequence &in_buffers,
                       const MutableBufferSequence &out_buffers,
                       boost::system::error_code &ec) {
        std::size_t read = 0;
        typename ConstBufferSequence::const_iterator iit;
        typename MutableBufferSequence::const_iterator oit;
        for (iit = in_buffers.begin(), oit = out_buffers.begin();
             iit != in_buffers.end() && oit != out_buffers.end();
             ++iit, ++oit) {
            speex_bits_reset(&bits_);
            speex_encode_int(enc_state_, boost::asio::buffer_cast<int16_t *>(*iit), &bits_);
            int size = speex_bits_write(
                    &bits_,
                    boost::asio::buffer_cast<char *>(*oit),
                    (int) boost::asio::buffer_size(*oit));
            read += size;
        }
        ec = boost::system::error_code();
        return read;
    }

private:
    void *enc_state_;
    SpeexBits bits_;
};

template<typename Service>
class BaseSpeexEncoder
        : public boost::asio::basic_io_object<Service> {
public:
    explicit BaseSpeexEncoder(boost::asio::io_service &io_service)
            : boost::asio::basic_io_object<Service>(io_service) {}

    /**
     * 异步编码
     * @tparam MutableBufferSequence
     * @tparam ReadHandler
     * @param buffers
     * @param handler
     */
    template<typename ConstBufferSequence, typename MutableBufferSequence, typename EncodeHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(EncodeHandler,
                                  void (boost::system::error_code, std::size_t))
    async_encode(const ConstBufferSequence &in_buffers,
                 const MutableBufferSequence &out_buffers,
                 BOOST_ASIO_MOVE_ARG(EncodeHandler)handler) {
        // If you get an error on the following line it means that your handler does
        // not meet the documented type requirements for a ReadHandler.
        using boost::asio::handler_type;
        BOOST_ASIO_READ_HANDLER_CHECK(EncodeHandler, handler) type_check;

        this->get_service().async_encode(this->get_implementation(),
                                         in_buffers,
                                         out_buffers,
                                         BOOST_ASIO_MOVE_CAST(EncodeHandler)(handler));
    }
};

template<typename Implementation = SpeexEncoderImpl>
class SpeexEncoderService
        : public boost::asio::io_service::service {
public:
    static boost::asio::io_service::id id;

    explicit SpeexEncoderService(boost::asio::io_service &owner)
            : boost::asio::io_service::service(owner),
              async_work_(new boost::asio::io_service::work(async_io_service_)),
              async_thread_(boost::bind(&boost::asio::io_service::run, &async_io_service_)) {}

    ~SpeexEncoderService() {
        async_work_.reset();
        async_io_service_.stop();
        async_thread_.join();
    }

    typedef boost::shared_ptr<Implementation> implementation_type;

    void construct(implementation_type &impl) {
        impl.reset(new Implementation);
    }

    void destroy(implementation_type &impl) {
        impl->destroy();
        impl.reset();
    }

    /**
     * 异步编码
     */
    template<typename EncodeHandler, typename ConstBufferSequence, typename MutableBufferSequence>
    class EncodeOperation {
    public:
        EncodeOperation(implementation_type &impl,
                        boost::asio::io_service &io_service,
                        const ConstBufferSequence &in_buffers,
                        const MutableBufferSequence &out_buffers,
                        EncodeHandler handler) :
                impl_(impl),
                io_service_(io_service),
                work_(io_service),
                in_buffers_(in_buffers),
                out_buffers_(out_buffers),
                handler_(handler) {
        }

        void operator()() const {
            implementation_type impl = impl_.lock();
            if (impl) {
                boost::system::error_code ec;
                std::size_t transferred = impl->encode(in_buffers_, out_buffers_, ec);
                this->io_service_.post(boost::asio::detail::bind_handler(
                        handler_, ec, transferred));
            } else {
                this->io_service_.post(boost::asio::detail::bind_handler(
                        handler_, boost::asio::error::operation_aborted, 0));
            }
        }

    private:
        boost::weak_ptr<Implementation> impl_;
        boost::asio::io_service &io_service_;
        boost::asio::io_service::work work_;
        const ConstBufferSequence in_buffers_;
        const MutableBufferSequence out_buffers_;
        EncodeHandler handler_;
    };

    template<typename ConstBufferSequence, typename MutableBufferSequence, typename EncodeHandler>
    BOOST_ASIO_INITFN_RESULT_TYPE(EncodeHandler,
                                  void (boost::system::error_code, std::size_t))
    async_encode(implementation_type &impl,
                 const ConstBufferSequence &in_buffers,
                 const MutableBufferSequence &out_buffers,
                 BOOST_ASIO_MOVE_ARG(EncodeHandler)handler) {

        using boost::asio::handler_type;
        typedef BOOST_ASIO_HANDLER_TYPE(
                EncodeHandler,
                void(boost::system::error_code, std::size_t)) encode_handler_type;

        boost::asio::detail::async_result_init<
                EncodeHandler, void(boost::system::error_code, std::size_t)> init(
                BOOST_ASIO_MOVE_CAST(EncodeHandler)(handler));

        this->async_io_service_.post(
                EncodeOperation<encode_handler_type, ConstBufferSequence, MutableBufferSequence>(
                        impl, this->get_io_service(), in_buffers, out_buffers, init.handler));

        return init.result.get();
    }

private:
    void shutdown_service() override {
    }

    boost::asio::io_service async_io_service_;
    boost::scoped_ptr<boost::asio::io_service::work> async_work_;
    boost::thread async_thread_;
};

template<typename SpeexEncoderImpl>
boost::asio::io_service::id SpeexEncoderService<SpeexEncoderImpl>::id;

typedef BaseSpeexEncoder<SpeexEncoderService<SpeexEncoderImpl>> SpeexEncoder;

}

#endif //SIMONE_SPEEX_ENCODER_H

#pragma clang diagnostic pop