//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "iat_session.h"

#include <boost/algorithm/string/join.hpp>
// #include <beast/websocket.hpp>
// #include <beast/websocket/ssl.hpp>
#include <json.hpp>
#include <boost/format.hpp>
#include "base64.h"
#include "util.h"

using json = nlohmann::json;

IATSession::IATSession(boost::asio::io_service &ios)
        : io_service_(ios),
          audio_stream_(ios),
          encoder_(ios),
          vad_detector_(ios),
          state_("idle") {

    // Initialize the Asio transport policy
    client_.init_asio(&io_service_);
}

void IATSession::connect(get_result_handler handler) {
    auto self(shared_from_this());

    get_result_handler_ = handler;

    client_.clear_access_channels(websocketpp::log::alevel::all);
    client_.set_open_handler([self](websocketpp::connection_hdl conn) {
        std::cout << " Speech 服务链路已启动" << std::endl;
        boost::posix_time::milliseconds interval(1000); // 1 second
        boost::asio::deadline_timer timer(self->io_service_, interval);
        timer.async_wait([self, conn](const boost::system::error_code &ec) {
            self->start(conn);
        });
    });

    client_.set_close_handler([](websocketpp::connection_hdl conn) {
        std::cout << "on_close" << "\n";
    });

    client_.set_fail_handler([](websocketpp::connection_hdl conn) {
        std::cout << "on_fail" << "\n";
    });

    client_.set_message_handler([self](
            websocketpp::connection_hdl conn,
            websocketpp::client<websocketpp::config::asio_client>::message_ptr msg) {
        // std::cout << "on_message: " << msg->get_payload() << std::endl;
        auto o = json::parse(msg->get_payload());
        const std::string ret = o["ret"];
        const std::string sub = o["sub"];
        const std::string cmd = o["cmd"];
        if (cmd == "ssb") {
            if (ret == "0") {
                self->session_id_ = o["data"]["sid"];
                std::cout << "on_session_begin_response: " << self->session_id_ << std::endl;
                // on_session_begin_response(doc["data"]);

                // 异步读取
                self->async_read(conn);
            } else {
                // ERROR, END SESSION, TRY AGAIN
            }
        } else if (cmd == "auw") {
//            on_audio_write_response(doc["data"]);
        } else if (cmd == "grs") {
            if (self->state_ != "grs") {
                return;
            }
//            if (rec_state != recorderStatus.getResult) {//on status error
//                utils.log("GET RESULT ERROR.");
//                return;
//            }
//            var ret = obj.ret;
//
            auto result_status = o["data"]["rss"];
            auto result = o["data"]["iatrst"];

            bool is_end = (result_status == "5");
//            var append = function (str) {
//                if (str != null && str != undefined) {
//                    iatResult += str;
//                }
//            };

            self->result_ += result;

            if (!is_end) {//success but not end
//                iatEvent.getResult();
                boost::posix_time::milliseconds interval(200); // 100ms
                boost::asio::deadline_timer timer(self->io_service_, interval);
                timer.async_wait([self, conn](const boost::system::error_code &ec) {
                    self->get_result(conn);
                });
            } else {// got a final result
//                append(result);
//                callback.onResult(ret, iatResult);
                self->on_get_result(conn, self->result_);
                self->result_.clear();//本次会话结束
//                iatResult = "";
//                rec_state = recorderStatus.sessionEnd;
//                iatSessionEnd();
                self->state_ = "idle";
            }

        } else if (cmd == "sse") {
            // on_session_end_response(o["data"]);
        }
    });

    client_.set_tls_init_handler([](websocketpp::connection_hdl conn) {
        auto ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(
                boost::asio::ssl::context(boost::asio::ssl::context::tlsv1_client));
        try {
            ctx->set_options(boost::asio::ssl::context::default_workarounds);
            ctx->set_verify_mode(boost::asio::ssl::context::verify_none);
        } catch (std::exception &e) {
            std::cout << e.what() << std::endl;
        }
        return ctx;
    });

    websocketpp::lib::error_code ec;
    websocketpp::client<websocketpp::config::asio_tls_client>::connection_ptr conn(
            client_.get_connection("wss://h5.openspeech.cn/iat.do", ec));
    conn->set_tcp_post_init_handler([](websocketpp::connection_hdl conn) {
    });
    conn->append_header("origin", "http://127.0.0.1:8000");
    client_.connect(conn);
}

void IATSession::start(websocketpp::connection_hdl conn) {
    std::cout << "开始听写循环" << std::endl;

    std::vector<std::string> params;
    params.push_back("appid=58074175");
    params.push_back("appidkey=f23b15002be25605");
    params.push_back("lang=sms");
    params.push_back("acous=anhui");
    params.push_back("aue=speex-wb;-1");
    params.push_back("usr=mkchen");
    params.push_back("ssm=1");
    params.push_back("sub=iat");
    params.push_back("net_type=wifi");
    params.push_back("rse=utf8");
    params.push_back("ent=sms16k");
    params.push_back("rst=plain");
    params.push_back("auf=audio/L16;rate=16000");
    params.push_back("vad_enable=1");
    params.push_back("vad_timeout=5000");
    params.push_back("vad_speech_tail=500");
    params.push_back("compress=igzip");

    json o = {
            {"ver", "1.0"},
            {"sub", "iat"},
            {"cmd", "ssb"},
            {"msg", "request"},
            {"data",
                    {
                            {"params", boost::algorithm::join(params, ",")}
                    }
            },
    };

    std::string payload(o.dump());
    // std::cout << payload << std::endl;
    client_.send(conn, payload.data(), payload.length(), websocketpp::frame::opcode::text);

    state_ = "ssb";
    recording_ = true;
}

void IATSession::async_read(websocketpp::connection_hdl conn) {
    auto self(shared_from_this());
    audio_stream_.async_read_some(
            boost::asio::buffer(buf_),
            [self, conn](const boost::system::error_code &ec, size_t bytes_transferred) {
                self->on_audio_raw_frame(conn);
            });
}

void IATSession::on_audio_raw_frame(websocketpp::connection_hdl conn) {
    auto self(shared_from_this());
    std::cout << "RAW Audio : " << make_string(buf_) << std::endl;
    vad_detector_.async_detect(
            boost::asio::buffer(buf_),
            [self, conn](const boost::system::error_code &ec, size_t bytes_transferred) {
                self->on_vad_detect_end(conn);
                if (bytes_transferred > 0) {
                    std::cout << "VAD Detected !" << std::endl;
                    self->get_result(conn);
                }
            });
}

/**
 * VAD 判断
 */
void IATSession::on_vad_detect_end(websocketpp::connection_hdl conn) {
    auto self(shared_from_this());
    encoder_.async_encode(
            boost::asio::buffer(buf_),
            boost::asio::buffer(encoded_buf_),
            [self, conn](const boost::system::error_code &ec, size_t bytes_transferred) {
                self->on_audio_encoded_frame(conn, bytes_transferred);
            });
}

/**
 * Speex 已经编码完成，上传
 */
void IATSession::on_audio_encoded_frame(websocketpp::connection_hdl conn, size_t frame_size) {
    auto self(shared_from_this());
    std::vector<uint8_t> frame(frame_size);
    std::copy(encoded_buf_.begin(),
              encoded_buf_.begin() + frame_size,
              frame.begin());
//     std::cout << make_string(frame) << std::endl;
    std::cout << "Encoded Audio : " << make_string(frame) << std::endl;
    write_audio(conn, frame.data(), frame.size(), 2);
    io_service_.post([self, conn] { self->async_read(conn); });
}

void IATSession::get_result(websocketpp::connection_hdl conn) {
    recording_ = false;

    if (state_ == "auw") {
        uint8_t data[2] = {0};
        write_audio(conn, data, 2, 4);
        state_ = "grs";
    }

    if (state_ == "grs") {
        json o = {
                {"ver", "1.0"},
                {"sub", "iat"},
                {"cmd", "grs"},
                {"msg", "request"},
                {"sid", session_id_}
        };

        std::string payload(o.dump());
        client_.send(conn, payload.data(), payload.length(), websocketpp::frame::opcode::text);
    }
}

void IATSession::write_audio(websocketpp::connection_hdl conn, const uint8_t *data, size_t size, int state) {
    if (state_ == "grs") return;
    if (session_id_.empty()) return;

    state_ = "auw";
    if (state != 4) {
        send_buffer_.push_back(42);
        send_buffer_.insert(send_buffer_.end(), data, data + size);
    }

    if (send_buffer_.size() == (6 * 43) || state == 4) {
        // std::cout << "Upload audio : " << send_buffer_.size() << std::endl;
        synid_++;

        const std::vector<uint8_t> &buf = send_buffer_;
        json o = {
                {"ver",  "1.0"},
                {"sub",  "iat"},
                {"cmd",  "auw"},
                {"msg",  "request"},
                {"sid",  session_id_},
                {"data", {
                                 {"synid", (boost::format("%d") % synid_).str()},
                                 {"audiolen", (boost::format("%d") % buf.size()).str()},
                                 {"audiostatu", "2"},
                                 {"audio", encode64(buf)}
                         }
                },
        };

        std::string payload(o.dump());
        // std::cout << payload << std::endl;
        client_.send(conn, payload.data(), payload.length(), websocketpp::frame::opcode::text);
        send_buffer_.clear();
    }

    if (state == 4) {
        std::vector<uint8_t> empty;

        const std::vector<uint8_t> &buf = empty;
        json o = {
                {"ver",  "1.0"},
                {"sub",  "iat"},
                {"cmd",  "auw"},
                {"msg",  "request"},
                {"sid",  session_id_},
                {"data", {
                                 {"synid", (boost::format("%d") % synid_).str()},
                                 {"audiolen", (boost::format("%d") % buf.size()).str()},
                                 {"audiostatu", "2"},
                                 {"audio", encode64(buf)}
                         }
                },
        };

        std::string payload(o.dump());
        std::cout << payload << std::endl;
        client_.send(conn, payload.data(), payload.length(), websocketpp::frame::opcode::text);
    }
}

/**
 * Got IAT Result
 * @param conn
 * @param result
 */
void IATSession::on_get_result(websocketpp::connection_hdl conn, const std::string &result) {
    auto self(shared_from_this());

    if (get_result_handler_ != nullptr) {
        get_result_handler_(result);
    }

    boost::posix_time::milliseconds interval(1000); // 1 second
    boost::asio::deadline_timer timer(self->io_service_, interval);
    timer.async_wait([self, conn](const boost::system::error_code &ec) {
        self->start(conn);
    });
}