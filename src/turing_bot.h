//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedStructInspection"

#ifndef SIMONE_TURING_BOT_H
#define SIMONE_TURING_BOT_H

#include <memory>
#include <boost/process.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/process/pipe.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/asio.hpp>
#include <json.hpp>

class TuringBot : public std::enable_shared_from_this<TuringBot> {
public:
    typedef std::function<void(const std::string &)> ReadHandler;

    TuringBot(boost::asio::io_service &ios)
            : io_service_(ios),
              pipe_(boost::process::create_pipe()),
              sink_(pipe_.sink, boost::iostreams::close_handle),
              source_(pipe_.source, boost::iostreams::close_handle),
              pend_(io_service_, pipe_.source) {
    }

    void talk(const std::string &question, ReadHandler handler) {
        auto self(shared_from_this());
        std::vector<std::string> args;
        args.push_back("/usr/bin/curl");
        args.push_back("-s");
        args.push_back("-X");
        args.push_back("POST");
        args.push_back("-d");
        args.push_back("key=31be67c60c7f4e76ab6359db55ce2691&info=" + question + "&loc=北京&userid=1");
        args.push_back("http://www.tuling123.com/openapi/api");
        boost::process::execute(
                boost::process::initializers::set_args(args),
                boost::process::initializers::bind_stdout(sink_)
        );
        boost::asio::async_read_until(
                pend_, buffer_, "}",
                [self, handler](const boost::system::error_code &ec, std::size_t sz) {
                    boost::asio::streambuf::const_buffers_type bufs = self->buffer_.data();
                    std::string line(
                            boost::asio::buffers_begin(bufs),
                            boost::asio::buffers_begin(bufs) + sz);
                    std::cout << "GOT RESULT: " << line << std::endl;
                    if (handler != nullptr) {
                        auto o = nlohmann::json::parse(line);
                        handler(o["text"]);
                    }
                });
    }

private:
    boost::asio::io_service &io_service_;
    boost::process::pipe pipe_;
    boost::iostreams::file_descriptor_sink sink_;
    boost::iostreams::file_descriptor_source source_;
    boost::asio::posix::stream_descriptor pend_;
    boost::asio::streambuf buffer_;
};

#endif //SIMONE_TURING_BOT_H

#pragma clang diagnostic pop