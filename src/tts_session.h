//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SIMONE_TTS_SESSION_H
#define SIMONE_TTS_SESSION_H

#include <memory>
#include <boost/asio/io_service.hpp>
#include <websocketpp/client.hpp>
#include <websocketpp/config/asio_no_tls_client.hpp>

typedef std::function<void(const std::string &)> tts_result_handler;

class TTSSession : public std::enable_shared_from_this<TTSSession> {
public:
    TTSSession(boost::asio::io_service &ios);

    void start(const std::string &content, tts_result_handler handler);

private:
    boost::asio::io_service &io_service_;
    websocketpp::client<websocketpp::config::asio_client> client_;
    std::string content_;
    tts_result_handler handler_;
};

#endif //SIMONE_TTS_SESSION_H
