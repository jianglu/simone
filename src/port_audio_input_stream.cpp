//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <portaudio.h>
#include "port_audio_input_stream.h"

using namespace portaudio;

PortAudioInputStreamImpl::PortAudioInputStreamImpl() {
    const double RATE = 16000.0f;
    const int CHANNELS = 1;
    const int CHUNK_DURATION_MS = 20; // supports 10, 20 and 30 (ms)
    const u_long CHUNK_SIZE = u_long(RATE * CHUNK_DURATION_MS / 1000);

    std::cout << "PortAudio Version: " << Pa_GetVersionText() << std::endl;

    std::cout << "Initialize PortAudio ..." << std::endl;
    Pa_Initialize();

    int defaultHost = Pa_GetDefaultHostApi();
    const PaHostApiInfo *info = Pa_GetHostApiInfo(defaultHost);
    std::cout << "Default Host Api: " << info->name << std::endl;

    int device_count = Pa_GetDeviceCount();
    std::cout << "Device Count: " << device_count << std::endl;
    for (int i = 0; i < device_count; i++) {
        const PaDeviceInfo *deviceInfo = Pa_GetDeviceInfo(i);
        std::cout << "Device [" << i << "] : " << deviceInfo->name << std::endl;
    }

    int defaultInput = Pa_GetDefaultInputDevice();
    if (defaultInput == paNoDevice) {
        defaultInput = 0;
    }

    const PaDeviceInfo *deviceInfo = Pa_GetDeviceInfo(defaultInput);
    if (deviceInfo != nullptr) {
        std::cout << "Default Device: " << deviceInfo->name << std::endl;
    } else {
        std::cout << "Default Device: NULL" << std::endl;
    }

    PaStreamParameters parameters;
    parameters.device = defaultInput;
    parameters.channelCount = CHANNELS;
    parameters.sampleFormat = paInt16;
    parameters.suggestedLatency = deviceInfo->defaultLowInputLatency;
    parameters.hostApiSpecificStreamInfo = NULL;

    // PaStream *stream = NULL;
    Pa_OpenStream(&stream_, &parameters, NULL, RATE, CHUNK_SIZE, paNoFlag, NULL, NULL);
    Pa_StartStream(stream_);
}

void PortAudioInputStreamImpl::read_frame(int16_t *buf, size_t frames) {
    Pa_ReadStream(stream_, buf, frames);
}

PortAudioInputStreamImpl::~PortAudioInputStreamImpl() {
    std::cout << "Terminate PortAudio ..." << std::endl;
    Pa_StopStream(stream_);
    Pa_Terminate();
}
