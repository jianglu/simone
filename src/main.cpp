//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <boost/asio.hpp>
#include <iostream>
#include <json.hpp>
#include "base64.h"
#include "tts_session.h"
#include "iat_session.h"
#include "emoti_bot.h"
#include "mad_play.h"

using json = nlohmann::json;

// UserId: 06ABDCE693A8CA63F7EF0850CB3883F5A

int main() {
    boost::asio::io_service ios;
    std::cout << json {{"pi", "Hello, World!"}} << std::endl;

//    session->start("引擎切换成功，当前是图灵机器人", [](const std::string &voiceUrl) {
//        std::cout << voiceUrl << std::endl;
//    });

    std::shared_ptr<IATSession> iat_session(new IATSession(ios));
    iat_session->connect([&ios](const std::string &result) {
        if (result.length() < 2) return;
        std::cout << "IAT: " << result << std::endl;
        // std::shared_ptr<TuringBot> bot(new TuringBot(ios));
        std::shared_ptr<EmotiBot> bot(new EmotiBot(ios));
        bot->talk(result, [&ios](const std::string &result) {
            std::cout << "BOT: " << result << std::endl;
            std::shared_ptr<TTSSession> tts_session(new TTSSession(ios));
            tts_session->start(result, [&ios](const std::string &result) {
                std::cout << "TTS: " << result << std::endl;
                std::shared_ptr<MadPlay> mad_play(new MadPlay(ios));
                mad_play->play(result, [] {
                    std::cout << "Complete !!" << std::endl;
                });
            });
        });
    });

    ios.run();
    return 0;
}