//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <json.hpp>
#include "tts_session.h"

using json = nlohmann::json;

TTSSession::TTSSession(boost::asio::io_service &ios)
        : io_service_(ios) {
    client_.init_asio(&io_service_);

    client_.clear_access_channels(websocketpp::log::alevel::all);
    client_.set_close_handler([this](websocketpp::connection_hdl conn) {
        std::cout << "tts on_close" << "\n";
    });

    client_.set_fail_handler([this](websocketpp::connection_hdl conn) {
        std::cout << "tts on_fail" << "\n";
    });

    client_.set_message_handler([this](
            websocketpp::connection_hdl conn,
            websocketpp::client<websocketpp::config::asio_client>::message_ptr msg) {
        std::cout << "tts on_message: " << msg->get_payload() << std::endl;

        // {"ver":"1.0","sub":"tts","cmd":"","msg":"response","ret":"0","synid":"12345","type":"","data":{"audioKey":"72327373f3b42c9d9a9b70adc973bc5e.mp3","duration":"3.78s","sid":"whs00000069@ch4754586bb93423c501","stat":"2"}}
        auto o = json::parse(msg->get_payload());
        std::string audio_key = o["data"]["audioKey"];
        if (handler_ != nullptr) {
            handler_("http://h5.xf-yun.com/audioStream/" + audio_key);
        }
        websocketpp::lib::error_code ec;
        client_.close(conn, websocketpp::close::status::normal, "", ec);
    });
}

void TTSSession::start(const std::string &content, tts_result_handler handler) {
    auto self(shared_from_this());
    client_.set_open_handler([self](websocketpp::connection_hdl conn) {
        std::cout << " TTS 服务链路已启动" << std::endl;
        json o = {
                {"ver",    "1.0"},
                {"sub",    "tts"},
                {"cmd",    "ssb"},
                {"synid",  "12345"},
                {"msg",    "request"},
                {"appid",  "58074175"},
                {"appkey", "f23b15002be25605"},
                {"data",
                           {
                                   {"params", "ent=aisound,appid=56fdf29f,aue=lame,vcn=aisxwz"},
                                   {"text", self->content_}
                           }
                }
        };
        std::string payload(o.dump());
        std::cout << payload << std::endl;
        self->client_.send(conn, payload.data(), payload.length(), websocketpp::frame::opcode::text);
    });

    websocketpp::lib::error_code ec;
    websocketpp::client<websocketpp::config::asio_client>::connection_ptr conn(
            client_.get_connection("ws://h5.xf-yun.com/tts.do", ec));
    conn->append_header("origin", "http://127.0.0.1:8000");
    client_.connect(conn);

    content_ = content;
    handler_ = handler;
}
