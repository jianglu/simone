//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "speex_encoder.h"

using namespace speex;

SpeexEncoderImpl::SpeexEncoderImpl() {
    speex_bits_init(&bits_);
    const SpeexMode *mode = speex_lib_get_mode(SPEEX_MODEID_WB);
    enc_state_ = speex_encoder_init(mode);
    int frame_size = 0;
    int quality = 5;
    speex_encoder_ctl(enc_state_, SPEEX_GET_FRAME_SIZE, &frame_size);
    speex_encoder_ctl(enc_state_, SPEEX_SET_QUALITY, &quality);
    std::cout << "Speex frame_size = " << frame_size << std::endl;
}

SpeexEncoderImpl::~SpeexEncoderImpl() {
    speex_encoder_destroy(enc_state_);
}

