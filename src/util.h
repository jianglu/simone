//
// Copyright 2017 "Jiang Lu <droidream@gmail.com>"
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SIMONE_UTIL_H
#define SIMONE_UTIL_H

#include <cstdint>
#include <boost/format.hpp>
#include <sstream>

template<typename _Tp>
std::string make_string(_Tp &t) {
    // return {t.begin(), t.end()};
    std::stringstream ss;
    ss << "<Buffer";
    for (auto i = t.begin(); i != t.end(); i++) {
        ss << boost::format(" %|02X|") % (int) (*i);
    }
    ss << ">";
    return std::move(ss.str());
}

#endif //SIMONE_UTIL_H
